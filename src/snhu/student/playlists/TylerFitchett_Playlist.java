package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class TylerFitchett_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> greenDayTracks = new ArrayList<Song>();
    GreenDay greenDay = new GreenDay();
	
    greenDayTracks = greenDay.getGreenDaySongs();
	
	playlist.add(greenDayTracks.get(0));
	playlist.add(greenDayTracks.get(1));
	
	ArrayList<Song> redHotChiliPeppersTracks = new ArrayList<Song>();
	RedHotChiliPeppers redHotChiliPeppers = new RedHotChiliPeppers();
	
    redHotChiliPeppersTracks = redHotChiliPeppers.getRedHotChiliPeppersSongs();
    
	playlist.add(redHotChiliPeppersTracks.get(0));
	playlist.add(redHotChiliPeppersTracks.get(1));
	playlist.add(redHotChiliPeppersTracks.get(2));
	
	
    return playlist;
	}
}